Яндекс карты
============
Удобное использование Яндекс карт.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist solovyevlv/yii2-ymap "*"
```

or add

```
"solovyevlv/yii2-ymap": "*"
```

to the require section of your `composer.json` file.

Добавить в composer.json в секцию repositories

```
"repositories" : [
        {
            "type" : "git",
            "url" : "https://solovjov_@bitbucket.org/solovjov_/yii2-ymap.git"
        }
    ],

```

Примеры использования
-----

Инициализация карты:

```
<?php
use solovyevlv\ymap\YMap;
use solovyevlv\ymap\Map;

$map = new Map(
    'map', 
    [
        'type' => Map::TYPE_PUB,
        'zoom' => 10,
        'controls' => [
            Map::CTRL_GEOLOC,
            Map::CTRL_TRAFIK
        ],
    ],
    [
        'scrollZoomSpeed' => 0.7
    ]
);

```

Добавление метки:

```
<?php

$map->addGeoObject(new \solovyevlv\ymap\objects\Placemark(
        [55.75, 37.80],
        [
            'hintContent' => 'Подсказка!',
        ],
        [
            'iconLayout' => 'default#image',
            'hideIcon' => false,
            'closeButton' => true,
            'iconImageHref' => $img_path,
            'iconImageSize' => [40, 40],
            'iconImageOffset' => [-15, -29],
            'openEmptyBalloon' => true,
            'balloonPanelMaxMapArea' => 0,
            'balloonMaxWidth' => 270,
        ]
    )
);

```

Добавление полигона:

```
<?php

$map->addGeoObject(new \solovyevlv\ymap\objects\Polygon([
        [
            [55.75,37.80],
            [55.80,37.90],
            [55.75,38.00],
            [55.70,38.00],
            [55.70,37.80]
        ]
    ],
    [
        'hintContent' => 'Многоугольник',
    ], 
    [
        'fillColor' => '#00ff00',
        'strokeWidth' => 2,
        'opacity' => 0.6
    ])
);

```

Добавление маршрута:

```
<?php

$map->addGeoObject(new \solovyevlv\ymap\objects\Router([
        'referencePoints' => [
            [55.734876, 37.59308],
            "Москва, ул. Мясницкая"
        ],
    ],
    [
        'routeStrokeWidth' => 2,
        'routeStrokeColor' => '#00ff88',
        'routeActiveStrokeWidth' => 6,
        'routeActiveStrokeColor' => '#00ff00',
    ])
);

```

Добавление коллекций:

```
<?php

$pm = [];

foreach($placemarks as $placepark) {
    $pm[] = new \solovyevlv\ymap\objects\Placemark(
        [$placepark['latitude'], $placepark['longitude']],
        [
            'hintContent' => 'Подсказка!',
        ],
        [
            'hideIcon' => false,
            'closeButton' => true,
            ...
        ]
    );
}

$collection = new \solovyevlv\ymap\objects\Collection($pm, [....]);

$map->addGeoObject($collection);
```

Вывод карты:

```
<?= (new YMap)->create($map,[
    'style' => [
        'height' => '500px', 
        'widht' => '600px'
    ]
]) ?>
```
Если есть необходимость загружать карту ajax:

```
<?= (new YMap([
    'ajax' => true,
    'timeout' => 2000, //мс
    'custom_js' => "MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<div style=\"color: #FFFFFF; font-weight: bold;\">$[properties.iconContent]</div>');",
    'preload' => '<img src="1.gif">'
]))->create($map,$params) ?>
```