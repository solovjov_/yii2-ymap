<?php

namespace solovyevlv\ymap;

use solovyevlv\ymap\objects\MapObject;

class Map extends MapObject implements interfaces\MapControl
{
    const TYPE_SAT = 'yandex#satellite';
    
    const TYPE_HYB = 'yandex#hybrid';
    
    const TYPE_MAP = 'yandex#map';
    
    const TYPE_PUB = 'yandex#publicMap';
    
    private $default_options = [
        'center'   => [55.76, 37.64],
        'controls' => [],
        'zoom'     => 10
    ];
    
    public function __construct($id, $options = [], $params = [])
    {
        if(!empty($options)) {
            $options = array_merge($this->default_options, $options);
        }
        
        $this->id = $id;
        
        parent::__construct($options, $params);
        
        $this->js = "var $id = new ymaps.Map('$id', $this->options, $this->params);\n";
        
    }
    
    public function addGeoObject(MapObject $object)
    {
        $this->js .= $object->buildJs();
        
        $this->js .= "$this->id.geoObjects.add($object->id);\n";

        return $this;
    }
    
    public function getJs()
    {
        return $this->buildJs();
    }
    
}
