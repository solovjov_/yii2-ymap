<?php

namespace solovyevlv\ymap\interfaces;

interface MapControl
{
    const CTRL_GEOLOC = 'geolocationControl';
    
    const CTRL_TRAFIK = 'trafficControl';
    
    const CTRL_ZOOM   = 'zoomControl';
    
    const CTRL_SCREEN = 'fullscreenControl';
    
    const CTRL_ROUTE  = 'routeEditor';
    
    const CTRL_SEARCH = 'searchControl';

    const CTRL_SMALL_DEFAULT = 'smallMapDefaultSet';
    
    const CTRL_LARGE_DEFAULT = 'largeMapDefaultSet';
}
