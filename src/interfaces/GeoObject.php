<?php

namespace solovyevlv\ymap\interfaces;

interface GeoObject 
{
    public function buildJs();
    
    public function bindEvent($event);
}
