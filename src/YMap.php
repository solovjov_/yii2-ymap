<?php

namespace solovyevlv\ymap;

use Yii;
use yii\helpers\Html;
use yii\base\BaseObject;

class YMap extends BaseObject
{
    public $uri = 'https://api-maps.yandex.ru';

    public $api_version = '/2.1';

    public $language = '/?lang=ru-RU';
    
    public $preload = '';
    
    public $ajax;

    public $custom_js = '';

    public $timeout = 1000;
    
    private $view;
    
    private $output;
    
    public function  init()
    {
        $url = $this->uri.$this->api_version.$this->language;
        
        if($this->ajax) {
            $this->output = Html::jsFile($url);
        } else {
            $this->view = Yii::$app->getView();

            $this->view->registerJsFile($url);
        }
    }
    
    public function create(Map $map, $params=[])
    {
        $js = "ymaps.ready(function() {\n{$this->custom_js}\n{$map->getJs()}\n});";
        
        if($this->ajax) {
            $js = "setTimeout(function() {{$js}},{$this->timeout})";
            $this->output .= "<script>$js</script>";
        } else {
            $this->view->registerJs($js);
        }

        $params = array_merge(['id' => $map->id], $params);

        $this->output .= Html::tag('div', $this->preload, $params);
        
        return $this->output;
    }
}
