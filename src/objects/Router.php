<?php

namespace solovyevlv\ymap\objects;

class Router extends MapObject 
{
    public $id = 'route';

    public function __construct($options = [], $params = []) 
    {
        parent::__construct($options, $params);

        $this->js = "var $this->id = new ymaps.multiRouter.MultiRoute($this->options, $this->params);\n";
    }

}
