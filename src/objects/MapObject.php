<?php

namespace solovyevlv\ymap\objects;

use yii\helpers\Json;
use solovyevlv\ymap\interfaces\GeoObject;

abstract class MapObject implements GeoObject 
{
    protected $js;
    
    protected $params;
    
    protected $options;
    
    public $id;
    
    public function __construct($options, $params = [])
    {
        $this->options = !empty($options) ? Json::encode($options) : '{}';
        
        $this->params = !empty($params) ? Json::encode($params) : '{}';
    }
    
    public function buildJs() 
    {
        return $this->js;
    }
    
    public function bindEvent($event) 
    {
        foreach($event as $e => $func) {
            $this->js .= "$this->id.events.add('$e', $func);\n";
        }
    }
}
