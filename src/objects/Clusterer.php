<?php

namespace solovyevlv\ymap\objects;

class Clusterer extends MapObject
{
    public $id = 'clusterer';
    
    public function __construct($items, $options)
    {
        parent::__construct($options);

        $this->js = "var $this->id = new ymaps.Clusterer($this->options);\n";
        
        $this->js .= "var points = [];\n";

        $this->fillPoints($items);
        
        $this->js .= "$this->id.add(points);\n";
    }
    
    private function fillPoints($items)
    {
        foreach ($items as $k => $item) {
            if ($item instanceof Placemark) {
                $this->js .= $item->buildJs();
                $this->js .= "points[$k] = $item->id;\n";
            }
        }
    }
    
}
