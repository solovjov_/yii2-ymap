<?php

namespace solovyevlv\ymap\objects;

use yii\helpers\Json;

class Polygon extends MapObject
{
    public $id = 'polygon';
    
    public function __construct($pos, $options=[], $params=[])
    {
        parent::__construct($options, $params);
        
        $pos_json = Json::encode($pos);
        
        $this->js = "var $this->id = new ymaps.Polygon($pos_json, $this->options, $this->params);\n";
        
        if(isset($params['events'])) {
            $this->bindEvent($params['events']);
        }
    }
    
}
