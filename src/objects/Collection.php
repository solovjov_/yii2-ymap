<?php

namespace solovyevlv\ymap\objects;

use solovyevlv\ymap\interfaces\GeoObject;

class Collection extends MapObject 
{
    public $id = 'collection';
    
    public function __construct(Array $items, $options = []) 
    {
        parent::__construct($options);
        
        $this->js = "var $this->id = new ymaps.GeoObjectCollection();\n";
        
        foreach ($items as $item) {
            $this->createJs($item);
        }
    }
    
    private function createJs(GeoObject $item)
    {
        $this->js .= $item->buildJs() . "\n";
        $this->js .= "collection.add({$item->id});\n";
    }

}
